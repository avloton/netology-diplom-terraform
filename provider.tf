terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.85.0"
    }
  }
}

provider "yandex" {
  cloud_id  = "b1gr44d4i93lihefk8bs"
  folder_id = "b1goj6apkc57lq3tc4cf"
}
