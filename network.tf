resource "yandex_vpc_network" "lab-net" {
  name = "network-${terraform.workspace}"
}

resource "yandex_vpc_subnet" "subnet-a" {
  name = "subnet-a-${terraform.workspace}"
  v4_cidr_blocks = ["192.168.${local.subnet_a[terraform.workspace]}.0/24"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.lab-net.id
}

resource "yandex_vpc_subnet" "subnet-b" {
  name = "subnet-b-${terraform.workspace}"
  network_id     = yandex_vpc_network.lab-net.id
  v4_cidr_blocks = ["192.168.${local.subnet_b[terraform.workspace]}.0/24"]
  zone           = "ru-central1-b"
}

resource "yandex_vpc_subnet" "subnet-c" {
  name = "subnet-c-${terraform.workspace}"
  network_id     = yandex_vpc_network.lab-net.id
  v4_cidr_blocks = ["192.168.${local.subnet_c[terraform.workspace]}.0/24"]
  zone           = "ru-central1-c"
}