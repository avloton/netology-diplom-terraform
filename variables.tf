locals {
  memory_map = {
    stage = 2
    prod = 2
  }
  core_map = {
    stage = 2
    prod = 2
  }
  subnet_a = {
    stage = 1
    prod = 10
  }
  subnet_b = {
    stage = 2
    prod = 20
  }
  subnet_c = {
    stage = 3
    prod = 30
  }
}