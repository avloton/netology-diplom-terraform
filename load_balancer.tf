resource "yandex_lb_target_group" "target_group" {
  name      = "target-group-${terraform.workspace}"
  region_id = "ru-central1"

  target {
    subnet_id = yandex_vpc_subnet.subnet-a.id
    address   = yandex_compute_instance.instance-a.network_interface.0.ip_address
  }

  target {
    subnet_id = yandex_vpc_subnet.subnet-b.id
    address   = yandex_compute_instance.instance-b.network_interface.0.ip_address
  }

  target {
    subnet_id = yandex_vpc_subnet.subnet-c.id
    address   = yandex_compute_instance.instance-c.network_interface.0.ip_address
  }
}

resource "yandex_lb_network_load_balancer" "load_balancer" {
  name = "network-load-balancer-${terraform.workspace}"
  region_id = "ru-central1"

  listener {
    name = "listener-${terraform.workspace}"
    port = 80
    target_port = 30000
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.target_group.id
    healthcheck {
      name = "tcp"
      tcp_options {
        port = 30000
      }
    }
  }
}

resource "yandex_lb_network_load_balancer" "load_balancer-grafana" {
  name = "network-load-balancer-grafana-${terraform.workspace}"
  region_id = "ru-central1"

  listener {
    name = "listener-grafana-${terraform.workspace}"
    port = 80
    target_port = 30001
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.target_group.id
    healthcheck {
      name = "tcp"
      tcp_options {
        port = 30001
      }
    }
  }
}