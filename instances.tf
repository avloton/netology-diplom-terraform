resource "yandex_compute_instance" "instance-a" {
  name = "node1-${terraform.workspace}"
  zone = "ru-central1-a"
  boot_disk {
    initialize_params {
      image_id = "fd8cqj9qiedndmmi3vq6"
      size = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-a.id
    nat = true
    ip_address = "192.168.${local.subnet_a[terraform.workspace]}.10"
  }
  resources {
    cores  = local.core_map[terraform.workspace]
    memory = local.memory_map[terraform.workspace]
  }
  metadata = {
    user-data = file("./meta.txt")
  }
}

resource "yandex_compute_instance" "instance-b" {
  name = "node2-${terraform.workspace}"
  zone = "ru-central1-b"
  boot_disk {
    initialize_params {
      image_id = "fd8cqj9qiedndmmi3vq6"
      size = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-b.id
    nat = true
    ip_address = "192.168.${local.subnet_b[terraform.workspace]}.11"
  }
  resources {
    cores  = local.core_map[terraform.workspace]
    memory = local.memory_map[terraform.workspace]
  }
  metadata = {
    user-data = file("./meta.txt")
  }
}

resource "yandex_compute_instance" "instance-c" {
  name = "node3-${terraform.workspace}"
  zone = "ru-central1-c"
  boot_disk {
    initialize_params {
      image_id = "fd8cqj9qiedndmmi3vq6"
      size = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-c.id
    nat = true
    ip_address = "192.168.${local.subnet_c[terraform.workspace]}.12"
  }
  resources {
    cores  = local.core_map[terraform.workspace]
    memory = local.memory_map[terraform.workspace]
  }
  metadata = {
    user-data = file("./meta.txt")
  }
}

resource "yandex_compute_instance" "gitlab-runner" {
  name = "gitlab-runner-${terraform.workspace}"
  zone = "ru-central1-a"
  boot_disk {
    initialize_params {
      image_id = "fd8cqj9qiedndmmi3vq6"
      size = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-a.id
    nat = true
    ip_address = "192.168.${local.subnet_a[terraform.workspace]}.100"
  }
  resources {
    cores  = 2
    memory = 2
  }
  metadata = {
    user-data = file("./meta.txt")
  }
}